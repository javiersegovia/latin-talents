<?php

add_action('admin_menu', 'create_employee_profile_setup');
 
function create_employee_profile_setup(){
  add_management_page( 'Create profiles', 'Create profiles', 'manage_options', 'admin_create_employee_profile', 'cep_init' );
}
 
function cep_init() {
  echo '<br>';
  echo "<h1>Create employee profiles</h1>";
  echo '<p>If you have imported new employees via CSV, this tool will generate their new profiles:</p>';

  $user_query = new WP_User_Query( array( 'role' => 'Employee' ) );
  $users = $user_query->get_results();
  $users_ids = array();
  if ( ! empty( $users ) ) {
    foreach ( $users as $user ) {
      $users_ids[] = $user->id;
      $usermeta = get_user_meta($user->id); 
    }
  } 
  else {
    echo '<p>We can\'t find any employee.</p>';
  }

  $employees_query = array(
    'posts_per_page' => -1,
    'post_type' => 'employee'
  );
  $employees = new WP_Query( $employees_query );
  $employees_ids = array();
  if ( $employees->have_posts() ) : 
    while ( $employees->have_posts() ) : $employees->the_post();
      array_push( $employees_ids, get_the_author_meta('ID') );
    endwhile;
  endif;

  $ids_to_create = array_diff($users_ids, $employees_ids);
  $n_of_e = count($ids_to_create);

  if ($ids_to_create) {
    foreach ( $ids_to_create as $e_id ) {
      $new_employee = get_userdata($e_id);
      createEmployee($new_employee);
    }
    echo '<p><strong>We have created ' . $n_of_e . ' employee profiles.</strong></p>';
  } else {
    echo '<p><strong>All your employees are up to date.</strong></p>';
  }
}