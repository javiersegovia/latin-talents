<?php

function register_contact_route() {
  register_rest_route('latintal/v1', 'manageContact', array(
    'methods' => 'POST',
    'callback' => 'updateContacts'
  ));
}
add_action('rest_api_init', 'register_contact_route');

function updateContacts($data) {
  
    $user_to_update = 'user_' . sanitize_text_field($data['currentUser']);
    $nc = sanitize_text_field($data['newContact']);
    $cc = sanitize_text_field($data['contactCount']);

    $contacts = get_field('users_contacted', $user_to_update);
    
    foreach($contacts as $contact) {
      if( in_array($nc, $contact) ) {
        return $nc . ', ' . $cc . ', ' . $user_to_update;
      }
    }

    $new_contact = array(
      'user_contacted' => $nc
    );
    
    if ($contacts) {
      array_push($contacts, $new_contact);
    } else {
      $contacts = array($new_contact);
    }
  
  // Update the repeater (contact list)
  update_field('field_5bce3204776fc', $contacts, $user_to_update);
  
  // Update the counter (contact limit)

  $cc++;
  update_field('field_5bccdd2fa5fd6', $cc, $user_to_update);

}
