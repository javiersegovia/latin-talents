<?php

function custom_post_types() {
  register_post_type('gallery', array(
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'supports' => array('title'),
      'labels' => array(
          'name' => 'Galleries',
          'singular_name' => 'Gallery',
          'add_new_item' => 'Add New Gallery',
          'edit_item' => 'Edit Gallery',
          'all_items' => 'All Galleries',
          'search_items' => 'Search Galleries'
      ),
      'menu_icon' => 'dashicons-format-gallery'
  ));
  register_post_type('employee', array(
      'public' => true,
      'show_ui' => true,
      'has_archive' => true,
      'show_in_menu' => true,
      'supports' => array('title', 'author'),
      'labels' => array(
          'name' => 'Employees',
          'singular_name' => 'Employee',
          'add_new_item' => 'Add New Employee',
          'edit_item' => 'Edit Employee',
          'all_items' => 'All Employees',
          'search_items' => 'Search Employees'
      ),
      'menu_icon' => 'dashicons-hammer'
  ));
}
add_action('init', 'custom_post_types');

function createGallery($uid, $unc) {
  wp_insert_post(array(
      'post_author' => $uid,
      'post_title' => $unc . '\'s gallery',
      'post_status' => 'publish',
      'post_type' => 'gallery',
      'meta_input' => array(
          'user_id__' => $uid
      )
  ));
}

function showGallery($perm, $gallery_id) {
  $post_id = $gallery_id[0];
  $images = get_field('gallery_image', $post_id);  
  if( $images ):
      $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
      foreach( $images as $image ): 
          echo wp_get_attachment_image( $image['ID'], $size ) . ' '; 
      endforeach;
  else:
    echo 'User has no gallery. Show default gallery instead.';
  endif;
  if ($perm):
      // Show edit buttons.
  endif; 
}

