<?php

add_role('employee', __(
    'Employee'),
    array(
    'read' => true, // Allows a user to read
    'create_posts' => true, // Allows user to create new posts
    'edit_posts' => true, // Allows user to edit their own posts
    )
);

function my_files() {

    $actualPage = get_post_field('post_name');
    if ( ! $actualPage && is_author()) {
        $actualPage = 'employee-page';
    }

    wp_enqueue_script('main_scripts', get_theme_file_uri('/js/scripts.js'), array('jquery'), null, true);
    wp_localize_script('main_scripts', 'cData', array(
        'siteURL' => get_site_url(),
        'actualPage' => $actualPage,
        'nonce' => wp_create_nonce('wp_rest')
    ));
}
add_action('wp_enqueue_scripts', 'my_files');

function manage_role_after_registration($user_id = 0, $i = array()) {

    if ($i['arm_action'] == 'please-signup' || $i['arm_action'] == 'employee-form') {
        $u = get_userdata($user_id);
        $u->set_role('employee');
        createEmployee($u);
    }
    elseif ($i['arm_action'] == 'membership-form') {
        $u = get_userdata($user_id);
        $u->set_role('armember');        
        update_field('field_5bccdd2fa5fd6', 0, 'user_' . $user_id);
    }
}

function createEmployee($user) {
    $uid = $user->ID;
    $unc = $user->user_nicename;
    $uname = $user->user_firstname . ' ' . $user->user_lastname;
    wp_insert_post(array(
        'post_author' => $uid,
        'post_title' => $uname,
        'post_name' => $unc,
        'post_status' => 'publish',
        'post_type' => 'employee',
        'meta_input' => array(
            'user_id__' => $uid
        )
    ));
}

add_action( 'arm_after_add_new_user', 'manage_role_after_registration', 10, 2);






