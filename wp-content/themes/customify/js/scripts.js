class ContactEmployee {
  constructor() {
    this.contactButton = document.querySelector('#makeContact');
    this.currentUser = this.contactButton.dataset.currentuser;
    this.newContact = this.contactButton.dataset.newcontact;
    this.contactCount = this.contactButton.dataset.contactcount;
    this.events();
  }

  events() {
    this.contactButton.addEventListener('click', () => {
      this.updateContacts(this.currentUser, this.newContact, this.contactCount)
    });
      
  }

  // methods

  updateContacts(currentUser, newContact, contactCount) {

    jQuery.ajax({
      url: cData.siteURL + '/wp-json/latintal/v1/manageContact',
      type: 'POST',
      data: {currentUser, newContact, contactCount},
    })
    .done(function(data) {

    })
    .fail(function(data) {

    });
  }
}

if (document.querySelector('#makeContact')) {
  const contactemployee = new ContactEmployee();
}




