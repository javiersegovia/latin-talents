<?php

global $post;
$a_id=$post->post_author;

$userdata = get_userdata($a_id);
$user_id = $userdata->id;
$usermeta = get_user_meta($user_id);

$current_user = '';
$current_user_meta = '';
$current_user_membership = '';

$current_user_is_employer = false;
$current_user_contacted = false;
$current_user_has_permissions = false;
$current_user_can_contact = false;

$current_user_contact_count = 0;
$current_user_membership_limit = 0;


if ( is_user_logged_in() ):
  $current_user = wp_get_current_user();
  $current_user_meta = get_user_meta($current_user->id);
  if ($user_id === $current_user->id || current_user_can('administrator')):
      $current_user_has_permissions = true;
  endif;
endif;

//   echo '<pre>'; print_r($usermeta); echo '</pre><br><br>';

  echo '<img src="' . $usermeta['avatar'][0] . '"/><br>';
  
  echo '<h2>Personal Info</h2>';
  echo '<strong>Nickname:</strong> ' . $usermeta['nickname'][0] . '<br>';
  echo '<strong>First Name:</strong> ' .$usermeta['first_name'][0] . '<br>';
  echo '<strong>Last Name:</strong> ' .$usermeta['last_name'][0] . '<br>';
  echo '<strong>Gender:</strong> ' .$usermeta['gender'][0] . '<br>';
  echo '<strong>Country:</strong> ' .$usermeta['country'][0] . '<br>';
  echo '<strong>Biography:</strong> ' .$usermeta['description'][0] . '<br><br>';

//   echo 'Role: ' .$usermeta['role'][0] . '<br>';
//   echo 'Is employee?: ' . $usermeta['is_employee'][0] . '<br>';

echo '<hr>';
echo '<h2>Other Info</h2>';
  
  if ($usermeta['arm_user_last_plan'][0] == 1) {
      echo '<strong>This user has the Free Membership.</strong><br>';
  } elseif ($usermeta['arm_user_last_plan'][0] == 2){
      echo '<strong>This user has the Premium Membership.</strong><br>';  
  } elseif ( ! $usermeta['arm_user_last_plan'][0] ) {
      echo '<strong>This user has no Membership.</strong><br>';
  }

  echo '<br><br><hr>';
  echo '<h2>About the visitor</h2>';

  if ( is_user_logged_in() ) {
    echo 'You are logged in as <strong>'  . $current_user_meta['nickname'][0] . '</strong><br>';
    
    if ($current_user_meta['arm_user_last_plan'][0] == 1) {
        echo 'You have the <strong>Free Membership.</strong><br>';

        $current_user_membership = 'free-membership';
        $current_user_membership_limit = 3;

    } elseif ($current_user_meta['arm_user_last_plan'][0] == 2){
        echo 'You have <strong>the Premium Membership.</strong><br>';  

        $current_user_membership = 'premium-membership';
        $current_user_membership_limit = 6;

    } elseif ( ! $current_user_meta['arm_user_last_plan'][0] ) {
          echo 'You have no membership.<br>';
      }

    $current_user_is_employer = in_array('armember', $current_user->roles);

    if ($current_user_is_employer) {
        
        $current_user_contact_count = (int) get_field('contact_count', 'user_' . $current_user->id);
        if ($current_user_contact_count || $current_user_contact_count === 0) {
            echo 'Current contact count: ' . $current_user_contact_count;
        } 
    }
    echo '<br><br>';
    echo '<h2>Contact</h2>';

    $people = get_field('users_contacted', 'user_' . $current_user->id);

    foreach($people as $person) {
        if( in_array($user_id, $person) ) {
            $current_user_contacted = true;
        }
    }

    if ($current_user_contacted || $current_user_has_permissions) {
        echo '<p>You have contacted this employee or you have proper permissions. You have access to the contact information:</p>';
        // Show contact info
        echo '<i>';
        echo '<strong>Phone:</strong> ' .$usermeta['phone'][0] . '<br>';
        echo '<strong>Email:</strong> ' . $userdata->user_email . '<br><br>';
        echo '</i>';
    }
    else {
        // Can the current user contact?
        if ($current_user_is_employer) { 
            echo '<p>You are an employer.</p>';
            if ($current_user_contact_count < $current_user_membership_limit) { // Check the current limit
                echo 'You can contact this employee. ';
                echo 'You have <strong>' . ($current_user_membership_limit - $current_user_contact_count) .  '</strong> contacts left.</p>';
                echo '<button type="button" id="makeContact" data-currentUser="' . $current_user->id . '" data-newContact="' . $user_id .'" data-contactCount="'. $current_user_contact_count .'">Make contact</button>';
                echo '<br>';
            } else {
                echo 'You can\'t contact this employee because you have reached the contact limit. Please renew your membership.';
            }
            echo '<br>';
        } else {
            echo '<p>You can\'t contact this employee. You are not an employer.</p>';
        }
    }  

    // echo '<pre>'; print_r($people); echo '</pre><br><br>';
    // echo '<pre>'; print_r($current_user_meta); echo '</pre><br><br>';
      
    } else {
        echo 'You are not logged in.';
    }
      
  echo '<h2>Gallery</h2>';

  $user_gallery_query = array(
    'post_type' => 'gallery',
    'fields' => 'ids',
    'meta_query' => array(
        array(
            'key' => 'user_id__',
            'compare' => '=',
            'value' => $user_id
        )
    )
  );

  $user_gallery = new WP_Query($user_gallery_query);
  $gallery_id = $user_gallery->posts;

  if ($user_gallery->found_posts) {
    showGallery($current_user_has_permissions, $gallery_id);
} else {
    if ($current_user_has_permissions) {
        createGallery($user_id, $userdata->user_nicename);
        $user_gallery = new WP_Query($user_gallery_query);
        $gallery_id = $user_gallery->posts;
    } 
    showGallery($current_user_has_permissions, $gallery_id); 
}

    echo '<br><br>';
    if ($current_user_has_permissions && $gallery_id):
        $post_id = $gallery_id[0];
        acf_form(array(
            'post_id'	=> $post_id,
            'post_title'	=> false,
            'submit_value'	=> 'Update the gallery',
            'updated_message' => false,
            'fields' => array(
                'field_5bc5f1fc93c1d'
            )
        )); 
    endif;
    
    

echo '<br><br><br>';

// if ( function_exists( 'mrp_rating_form' ) ) {
//     mrp_rating_form(array(
//         'post_id' => 1657 
//     ));

//     echo '<br><br><br>';
//     echo '2nd';

//     mrp_rating_form();
//   // you can also pass in parameters such as the the post id if needed e.g. mrp_rating_result( array( 'post_id' => $post_id ) );
// } else {
//     echo 'not getting it';
// }

// echo do_shortcode('[mr_rating_form]');

// $user_skills_query = array(
//     'post_type' => 'skills',
//     'fields' => 'ids',
//     'meta_query' => array(
//         array(
//             'key' => 'user_id__',
//             'compare' => '=',
//             'value' => $user_id
//         )
//     )
//   );

//   $user_skills = new WP_Query($user_skills_query);
//   $skills_id = $user_skills->posts;

//   if ($user_skills->found_posts) {
//     // echo spr_get_entry_rating($skills_id[0], false);
//     echo spr_show_rating();
// } else {
//     echo 'User doesnt have rating.';
// }


// echo do_shortcode('[yasr_visitor_multiset setid=0]');

if ($current_user_contacted || $current_user_has_permissions) {
    echo '<p>You have contacted this user or you have proper permissions. You can rate him now.</p>';
    echo do_shortcode('[yasr_visitor_votes]');
} else {
    echo '<p>You have to contact the user before you can rate him.</p>';
    echo do_shortcode('[yasr_visitor_votes_readonly size=”large”]');
}




// if ( function_exists('spr_show_rating') ) {
//     spr_get_entry_rating(1662, true);
// }

