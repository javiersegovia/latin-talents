<?php /* Template Name: Memberlist Page */

get_header(); ?>

<?php
$args = array(
    'role' => 'Employee'
);

// The Query
$user_query = new WP_User_Query( array( 'role' => 'Employee' ) );
$users = $user_query->get_results();

// User Loop
if ( ! empty( $users ) ) {
	foreach ( $users as $user ) {
    $usermeta = get_user_meta($user->id); ?>

    <h3>
    <a href="<?php echo get_site_url() . '/employee/' . strtolower($usermeta['nickname'][0]); ?>"><?php echo $usermeta['first_name'][0] . ' ' . $usermeta['last_name'][0]; ?></a>
    </h3>

    <?php

    echo '<strong>Avatar:</strong> ' .$usermeta['avatar'][0] . '<br>';
    echo '<strong>Nickname:</strong> ' . $usermeta['nickname'][0] . '<br>';
    echo '<strong>First Name:</strong> ' . $usermeta['first_name'][0] . '<br>';
    echo '<strong>Last Name:</strong> ' . $usermeta['last_name'][0] . '<br>';
    echo '<strong>Email:</strong> ' . $user->user_email . '<br>';
    echo '<strong>Joined:</strong> ' . $user->user_registered . '<br>';
    echo '<strong>Phone:</strong> ' .$usermeta['phone'][0] . '<br>';
    echo '<strong>Gender:</strong> ' .$usermeta['gender'][0] . '<br>';
    echo '<strong>Country:</strong> ' .$usermeta['country'][0] . '<br>';
    echo '<strong>Biography:</strong> ' . $usermeta['description'][0] . '<br>';

    echo '<br>';

	}
} else {
	echo 'No users found.';
}
?>

<?php get_footer();