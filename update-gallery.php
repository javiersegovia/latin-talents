
<?php
require_once('wp-load.php');

function RemapFilesArray($name, $type, $tmp_name, $error, $size) {
    return array(
        'name' => $name,
        'type' => $type,
        'tmp_name' => $tmp_name,
        'error' => $error,
        'size' => $size,
    );
}

function my_update_attachment($f,$pid,$t='',$c='') {
  wp_update_attachment_metadata( $pid, $f );
  if( !empty( $f['name'] )) { // New upload
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $override['test_form'] = false;
    $file = wp_handle_upload( $f, $override );

    if ( isset( $file['error'] )) {
      return new WP_Error( 'upload_error', $file['error'] );
    }

    $file_type = wp_check_filetype($f['name'], array(
      'jpg|jpeg' => 'image/jpeg',
      'gif' => 'image/gif',
      'png' => 'image/png',
    ));

    if ($file_type['type']) {
      $name_parts = pathinfo( $file['file'] );
      $name = $f['name'];
      $type = $file['type'];
      $title = $t ? $t : $name;
      $content = $c;

      $attachment = array(
        'post_title' => $title,
        'post_type' => 'attachment',
        'post_content' => $content,
        'post_parent' => $pid,
        'post_mime_type' => $type,
        'guid' => $file['url'],
      );
      echo 'attachment: <br>';
      print_r($attachment);

      foreach( get_intermediate_image_sizes() as $s ) {
        $sizes[$s] = array( 'width' => '', 'height' => '', 'crop' => true );
        $sizes[$s]['width'] = get_option( "{$s}_size_w" ); // For default sizes set in options
        $sizes[$s]['height'] = get_option( "{$s}_size_h" ); // For default sizes set in options
        $sizes[$s]['crop'] = get_option( "{$s}_crop" ); // For default sizes set in options
      }

      $sizes = apply_filters( 'intermediate_image_sizes_advanced', $sizes );

      foreach( $sizes as $size => $size_data ) {
        $resized = image_make_intermediate_size( $file['file'], $size_data['width'], $size_data['height'], $size_data['crop'] );
        if ( $resized )
          $metadata['sizes'][$size] = $resized;
      }

      $attach_id = wp_insert_attachment( $attachment, $file['file'] /*, $pid - for post_thumbnails*/);

      if ( !is_wp_error( $attach_id )) {
        $attach_meta = wp_generate_attachment_metadata( $attach_id, $file['file'] );
        wp_update_attachment_metadata( $attach_id, $attach_meta );
      }

   return array(
  'pid' =>$pid,
  'url' =>$file['url'],
  'file'=>$file,
  'attach_id'=>$attach_id
   );
    }
  }
}

function retrieve_att_ids($arr) {
    return $arr->id;
} 

if (isset($_FILES['upload_attachment'])){
    $files = array_map(
        'RemapFilesArray',
        $_FILES['upload_attachment']['name'],
        $_FILES['upload_attachment']['type'],
        $_FILES['upload_attachment']['tmp_name'],
        $_FILES['upload_attachment']['error'],
        $_FILES['upload_attachment']['size']
    );
    
    $gallery = array();
    $post_id = $_POST['upload_post_id'];
    $user_nicename = $_POST['upload_user_nicename'];
    
    foreach ($files as $file){ // loop through each file
        $att = my_update_attachment($file, $post_id);

        array_push($gallery,$att['attach_id']);
    }

    $att = my_update_attachment('upload_attachment', $post_id);

    $oldImages = get_field('gallery_image', $post_id);
    $op_ids = array_column($oldImages, 'id');
    $gallery = array_merge($gallery, $op_ids);

    // echo 'post_id' . $post_id;
    // echo '<h1>New pictures...</h1>';
    // print_r($gallery);
    // echo '<br><br><br>';

    // echo $post_id;
    // echo '<h1>Old pictures...</h1>';
    // echo '<pre>'; print_r($oldImages); echo '</pre><br><br>';
    // echo '<br><br><br>';

    // echo '<h1>Retrieve old pictures ID...</h1>';
    // echo '<pre>'; print_r($op_ids); echo '</pre><br><br>';
    // echo '<br><br><br>';


    // echo '<h1>New Array Gallery...</h1>';
    // echo '<pre>'; print_r($gallery); echo '</pre><br><br>';
    // echo '<br><br><br>';

    update_field('field_5bc5f1fc93c1d', $gallery, $post_id);
    header('Location: ' . site_url() . '/employee/' . $user_nicename);
}


?>