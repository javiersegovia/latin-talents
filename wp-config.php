<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'latintalents');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', 'utf8_general_ci');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kbY7@G]Hob-PutvWx(yOaVn6c<IN$us>`V;[:CB#y{DQ0XXm^%/dohx^L:bfT!LB');
define('SECURE_AUTH_KEY',  ';`5;^GPrb2c3&qVGH! O)W*RQq=i4Ne`*CrIxR<u@$h4|Ke}{vZ?<<FKL+S:IJs`');
define('LOGGED_IN_KEY',    'bf Mkgo`kf%HRef,u)xu q9f%WFU]031%gl3y<dzW$;jZ>{ RMWC[=`PYYQ^h!9)');
define('NONCE_KEY',        'eD -~^/5<:y{tU@FlOV7IjD#X2(*lA `9<UHOl3;#K/e3%-2]f0KViG,zV+_qhd5');
define('AUTH_SALT',        ' ixA8/c#W}A9^qEdp4xuaE+Il{WL:[g9TWw2R[C_1j^kZ^zk*iSdr^rTTn-eIW`H');
define('SECURE_AUTH_SALT', ';-z,J(wQ5^{-3h<S`$!wY1N?H4[vTm52|9mrU.O1y:5Nk6(nAM2+=IJ]Y|NQm(Wa');
define('LOGGED_IN_SALT',   'J3x4VXNyz_1{**_&3twF(Yus}.#^Ny4nF8h!r=1$P71w7AO~;/69BKi}t nhoAk$');
define('NONCE_SALT',       ')A>q$C>wOMF>]GI~vp&?.F{V^BSL7.e[.Pdc;F]<zm:?Gs9c`%rmF%BY@aSf4-*D');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '1sdf564fas8_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
